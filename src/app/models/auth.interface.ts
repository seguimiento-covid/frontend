export interface Auth {
  id?: number;
  dni: string;
  nombres?: string;
  apellidos?: string;
  hasCovid?: boolean;
  completado?: boolean;
  logueado?: boolean;
  password?: string;
  perfil?: string;
  token?: string;
  recordar?: boolean;
  perfilId: number;
  codigo?: number;
  estado?: number;
}
export interface RequestChangeClave {
  id?: number;
  claveAnterior?: string;
  claveNueva?: string;
}
export interface Respuesta {
  codigo?: number;
  mensaje?: string;
}
export interface ResponseInsert {
  id?: number;
  contraTemp: string;
  codigo: number;
  mensaje: string;
}
export interface ShowEdit {
  id: number;
  nombres: string;
  apellidos: string;
  dni: string;
  correo: string;
  telefono: string;
  direccion: string;
  perfilId: number;
  estado: string;
}
