export interface Sintomas {
  sintomaId: number;
  detalle?: string;
  otroDetalle?: string;
  checke?: boolean;
}
