import { Signos } from './signos.interface';
import { Sintomas } from './Sintomas.interface';
export interface ReportCovid {
  id?: number;
  sintomas: Sintomas[];
  temperatura: number;
  comentario: string;
  signos: Signos[];
  userId: number;
  fecha: string;
}
