export interface Personal {
  id?: number;
  dni?: string;
  nombre?: string;
  apellido?: string;
  telefono?: string;
  perfil?: string;
  idReporte?: number;
  fechaReporte: Date;
  cantDosis?: number;
  confirmado?: boolean;
  sospechozo?: boolean;
  positivo?: boolean;
  temperatura?: number;
  comentario?: string;
  correo?: string;
  direccion?: string;
  sintomas?: string[];
  signos?: string[];
  normal?: boolean;
}
