export interface Diario {
  id: number;
  dni: string;
  nombre: string;
  perfil: string;
  estado: number;
  descripcion: string;
  appId?: string;
}
