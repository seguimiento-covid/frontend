import { Sintomas } from './Sintomas.interface';

export interface Reporte {
  id?: number;
  contactoSospechozo: number;
  contactoConfirmado: number;
  positivo: number;
  cantDosis: number;
  userId: number;
  sintomas: Sintomas[];
  fecha: string;
}
