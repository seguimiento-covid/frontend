export interface Asignacion {
  id?: number;
  dni?: string;
  nombre?: string;
  perfil?: string;
  hasCovid?: boolean;
  userMaster?: number;
}
