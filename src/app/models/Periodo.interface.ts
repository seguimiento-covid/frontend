export interface Periodo {
  dni: string;
  nombre: string;
  perfil: string;
  dias: number[];
}
