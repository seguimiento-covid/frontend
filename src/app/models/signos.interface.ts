export interface Signos {
  signoId?: number;
  detalle?: string;
  otroDetalle?: string;
  checke?: boolean;
}
