import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ChangePassGuard } from './guards/change-pass.guard';
import { LoginGuard } from './guards/login.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./auth/login/login.module').then((m) => m.LoginPageModule),
    canActivate: [LoginGuard],
  },
  {
    path: 'control',
    loadChildren: () =>
      import('./components/reportes-adm/reportes-adm.module').then(
        (m) => m.ReportesAdmPageModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'cambiarClave',
    loadChildren: () =>
      import('./auth/change-pass/change-pass.module').then(
        (m) => m.ChangePassPageModule
      ),
    canActivate: [ChangePassGuard],
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: NotFoundComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
