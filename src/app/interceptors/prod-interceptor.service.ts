import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { TokenService } from '../services/token.service';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(
    private token: TokenService,
    private toast: ToastController,
    private authSvc: AuthService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.token.getToken()) {
      request = request.clone({
        setHeaders: {
          authorization: `Bearer ${this.token.getToken()}`,
        },
      });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json',
        },
      });
    }
    request = request.clone({
      headers: request.headers.set('Accept', 'application/json'),
    });
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => event),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        if (error.status === 401) {
          this.presentToast(
            'Expiró su token, loguése de nuevo por favor!',
            'warning'
          );
        }
        if (error.status === 0) {
          this.presentToast('Ocurrió un error en el servidor', 'warning');
        }
        return throwError(error);
      })
    );
  }
  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'bottom',
      buttons: [
        {
          text: 'Ir a inicio',
          role: 'cancel',
          handler: () => {
            this.authSvc.logout();
          },
        },
      ],
    });
    toast.present();
  }
}
