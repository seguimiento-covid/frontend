import { SessionService } from './../../../services/session.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/token.service';
import { NavController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() mensaje = '';
  currentPopover: any;
  constructor(
    private token: TokenService,
    private session: SessionService,
    private router: Router,
    private navRouter: NavController,
    private popoverController: PopoverController
  ) {}

  ngOnInit() {}
  getUrl() {
    return this.router.url;
  }
  obtenerUsuario() {
    return this.token.getDni();
  }
  logout() {
    this.token.logOut();
    this.session.clearAll();
    this.navRouter.navigateRoot('/login');
  }
  back() {
    this.navRouter.back();
  }
  abrirPefil(dato) {
    if (dato === 0) {
      this.router.navigate(['/perfil/ver']);
    } else {
      this.router.navigate(['/perfil/editar']);
    }
  }
}
