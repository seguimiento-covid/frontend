/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  columns = 12;
  mensaje = 'Bienvenido(a)';
  constructor(private token: TokenService) {}

  ngOnInit() {}

  verifyLogin() {
    return this.token.getDni();
  }
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', this.mensaje);
  }
  verificarCovid(): boolean {
    if (this.token.getHasCovid() !== 'no') {
      return true;
    } else {
      return false;
    }
  }
}
