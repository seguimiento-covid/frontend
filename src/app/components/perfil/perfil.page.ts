import { LoadingService } from './../../services/loading.service';
import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GestionUserService } from 'src/app/services/gestion-user.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  mensaje = '';
  perfilForm: FormGroup;
  accion: string;
  mensajeTitle = 'Menú de Perfil';
  disabled = true;
  changePass = false;
  disabledDni = true;
  hideTemp = true;
  hideNew = true;
  hideRepeat = true;
  inputs = [
    { nombre: 'Dni', formcontrol: 'dni', type: 'text', opc: 'user' },
    { nombre: 'Nombre', formcontrol: 'nombres', type: 'text', opc: 'user' },
    { nombre: 'Apellido', formcontrol: 'apellidos', type: 'text', opc: 'user' },
    { nombre: 'Correo', formcontrol: 'correo', type: 'text', opc: 'user' },
    { nombre: 'Telefono', formcontrol: 'telefono', type: 'text', opc: 'user' },
    {
      nombre: 'Dirección',
      formcontrol: 'direccion',
      type: 'text',
      opc: 'user',
    },
    {
      nombre: 'Clave Anterior',
      formcontrol: 'claveAnterior',
      type: 'text',
      opc: 'clave',
    },
    {
      nombre: 'Clave Nueva',
      formcontrol: 'claveNueva',
      type: 'text',
      opc: 'clave',
    },
    {
      nombre: 'Repetir Contraseña',
      formcontrol: 'direccion',
      type: 'text',
      opc: 'clave',
    },
  ];
  constructor(
    private fb: FormBuilder,
    private activeRoute: ActivatedRoute,
    private gestion: GestionUserService,
    private token: TokenService,
    private loading: LoadingService
  ) {
    this.accion = this.activeRoute.snapshot.paramMap.get('accion');

    if (this.accion === 'ver') {
      this.mensaje = 'Ver datos personales';
      this.disabled = true;
      this.changePass = false;
      this.perfilForm = this.fb.group({
        id: [this.token.getUserID()],
        dni: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        nombres: ['', [Validators.required]],
        apellidos: ['', [Validators.required]],
        telefono: [
          '',
          [
            Validators.required,
            Validators.maxLength(9),
            Validators.pattern('^[0-9]*$'),
          ],
        ],
        correo: ['', [Validators.required, Validators.email]],
        direccion: ['', [Validators.required]],
      });
    } else if (this.accion === 'editar') {
      this.mensaje = 'Editar datos personales';
      this.changePass = false;
      this.disabled = false;
      this.perfilForm = this.fb.group({
        id: [this.token.getUserID()],
        dni: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
        nombres: ['', [Validators.required]],
        apellidos: ['', [Validators.required]],
        telefono: [
          '',
          [
            Validators.required,
            Validators.maxLength(9),
            Validators.pattern('^[0-9]*$'),
          ],
        ],
        correo: ['', [Validators.required, Validators.email]],
        direccion: ['', [Validators.required]],
      });
    } else if (this.accion === 'changePass') {
      this.mensaje = 'Cambiar Contraseña';
      this.changePass = true;
      this.perfilForm = this.fb.group({
        id: [this.token.getUserID()],
        claveAnterior: ['', [Validators.required]],
        claveNueva: ['', [Validators.required]],
        confirm: ['', [Validators.required, this.matchValues('claveNueva')]],
      });
    }
  }

  ngOnInit() {}

  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Perfil');
    this.gestion.obtenerTrabajarPorId(this.token.getUserID()).subscribe(
      (data) => {
        this.perfilForm.patchValue({
          dni: data.dni,
          nombres: data.nombres,
          apellidos: data.apellidos,
          correo: data.correo.trim(),
          telefono: data.telefono,
          direccion: data.direccion,
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }
  dibujarForm() {
    if (this.changePass) {
      return this.inputs.filter((element) => element.opc === 'clave');
    } else {
      return this.inputs.filter((element) => element.opc === 'user');
    }
  }
  editarDatos() {
    this.loading.presentLoading('Validando...');
    if (this.perfilForm.valid) {
      this.gestion.editarTrabajadorNor(this.perfilForm.value).subscribe(
        (data) => {
          if (data.codigo === 200) {
            this.gestion.presentToast(data.mensaje, 'success');
          } else {
            this.gestion.presentToast(data.mensaje, 'danger');
          }
        },
        (err) => {
          this.loading.closeLoading();
          console.log(err);
        },
        () => {
          this.loading.closeLoading();
        }
      );
    } else {
      console.log(this.perfilForm.controls);
      this.gestion.presentToast('Complete todos los campos', 'danger');
      this.loading.closeLoading();
    }
  }
  cambiarClave() {
    this.loading.presentLoading('Validando...');
    if (this.perfilForm.valid) {
      this.gestion.cambiarClaveUser(this.perfilForm.value).subscribe(
        (data) => {
          if (data.codigo === 200) {
            this.gestion.presentToast(data.mensaje, 'success');
          } else {
            this.gestion.presentToast(data.mensaje, 'danger');
          }
        },
        (err) => console.log(err),
        () => {
          this.loading.closeLoading();
        }
      );
    } else {
      this.gestion.presentToast('Complete todos los campos', 'danger');
    }
  }
  matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null =>
      !!control.parent &&
      !!control.parent.value &&
      control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: false };
  }
  getError(nombre) {
    console.log(this.perfilForm.get(nombre).errors);
    return (
      this.perfilForm.get(nombre).touched && this.perfilForm.get(nombre).errors
    );
  }
}
