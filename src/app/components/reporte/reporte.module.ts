import { PagePartModule } from 'src/app/components/page-part/page-part.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportePageRoutingModule } from './reporte-routing.module';

import { ReportePage } from './reporte.page';
import { ReporteService } from 'src/app/services/reporte.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ReportePageRoutingModule,
    PagePartModule,
  ],
  declarations: [ReportePage],
  providers: [ReporteService],
})
export class ReportePageModule {}
