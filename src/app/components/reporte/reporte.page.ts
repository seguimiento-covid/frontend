import { LoadingService } from './../../services/loading.service';
/* eslint-disable @typescript-eslint/member-ordering */
import { ReporteService } from 'src/app/services/reporte.service';
import { Reporte } from 'src/app/models/Reporte.interface';
import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Sintomas } from 'src/app/models/Sintomas.interface';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.page.html',
  styleUrls: ['./reporte.page.scss'],
})
export class ReportePage implements OnInit {
  myVar: Date = new Date();
  sintomas: Sintomas[] = [];
  reporteForm: FormGroup;
  reporteFinal: Reporte;
  userId: string;
  constructor(
    private fb: FormBuilder,
    private toast: ToastController,
    private reporteService: ReporteService,
    private token: TokenService,
    private router: Router,
    private loadingSvc: LoadingService
  ) {
    this.loadingSvc.presentLoading('Cargando Reporte');
  }

  ngOnInit() {
    this.reporteService.obtenerSintomas().subscribe(
      (data) => {
        this.sintomas = data;
        this.loadingSvc.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loadingSvc.closeLoading();
      }
    );
    this.userId = this.token.getUserID();
    this.reporteForm = this.fb.group({
      sintomasss: this.fb.array([], [Validators.required]),
      casoSozpechoso: ['', [Validators.required]],
      casoConfirmado: ['', [Validators.required]],
      positivo: ['', [Validators.required]],
      cantDosis: ['', [Validators.required]],
      otroDetalle: [''],
    });
  }
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Formulario');
  }
  save() {
    if (this.reporteForm.valid) {
      this.reporteFinal = {
        id: 0,
        contactoSospechozo: this.reporteForm.get('casoSozpechoso').value,
        contactoConfirmado: this.reporteForm.get('casoConfirmado').value,
        positivo: this.reporteForm.get('positivo').value,
        cantDosis: this.reporteForm.get('cantDosis').value,
        userId: Number(this.userId),
        sintomas: this.devolverSintomas(),
        fecha: this.myVar.toJSON(),
      };
      this.reporteService.enviarReporte(this.reporteFinal);
      this.token.setCompleted('true');
      this.router.navigate(['/control/principal']);
      return true;
    } else {
      this.presentToast('Falta llenar datos', 'danger');
      this.reporteForm.markAllAsTouched();
    }
  }
  sintomasFinal: Sintomas[] = [];
  devolverSintomas(): Sintomas[] {
    this.sintomasFinal = [];
    this.sintomasFinal = this.reporteForm
      .get('sintomasss')
      .value.map((element) => {
        if (element === 14) {
          return {
            sintomaId: 14,
            otroDetalle: this.reporteForm.get('otroDetalle').value,
          };
        } else {
          return { sintomaId: element, otroDetalle: '' };
        }
      });
    return this.sintomasFinal;
  }
  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'top',
      duration: 4000,
    });
    toast.present();
  }
  onChangeBoxSintomas(e) {
    const isArray: FormArray = this.reporteForm.get('sintomasss') as FormArray;
    if (e.target.checked) {
      isArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      isArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          isArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
}
