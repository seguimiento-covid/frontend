import { LoadingService } from './../../../../services/loading.service';
import { GestionUserService } from './../../../../services/gestion-user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NavParams } from '@ionic/angular';
import { Perfil } from 'src/app/models/Perfil.interface';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss'],
})
export class EditarComponent implements OnInit {
  agregarForm: FormGroup;
  prefiles: Perfil[];
  id = 0;
  inputs = [
    { title: 'DNI', nameForm: 'dni', type: 'text', maximo: 8 },
    { title: 'Nombres', nameForm: 'nombres', type: 'text', maximo: 150 },
    { title: 'Apellidos', nameForm: 'apellidos', type: 'text', maximo: 150 },
    { title: 'Teléfono', nameForm: 'telefono', type: 'text', maximo: 9 },
    { title: 'Correo', nameForm: 'correo', type: 'text', maximo: 150 },
    { title: 'Dirección', nameForm: 'direccion', type: 'text', maximo: 250 },
  ];
  constructor(
    private navParams: NavParams,
    private fb: FormBuilder,
    private modalController: ModalService,
    private gestionSvc: GestionUserService,
    private loading: LoadingService
  ) {
    this.id = this.navParams.get('id');
    this.agregarForm = this.fb.group({
      id: [this.id],
      dni: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      telefono: [
        '',
        [
          Validators.required,
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      correo: ['', [Validators.required, Validators.email]],
      direccion: ['', [Validators.required]],
      perfilId: ['', [Validators.required]],
    });
    this.gestionSvc.obtenerTipoTrabajador().subscribe(
      (data) => (this.prefiles = data),
      (err) => console.log(err)
    );
    this.gestionSvc.obtenerTrabajarPorId(this.id).subscribe(
      (data) => {
        this.agregarForm.patchValue({
          dni: data.dni,
          nombres: data.nombres,
          apellidos: data.apellidos,
          telefono: data.telefono,
          correo: data.correo,
          direccion: data.direccion,
          perfilId: data.perfilId,
        });
      },
      (err) => {
        this.modalController.dismiss();
      }
    );
  }

  ngOnInit() {}
  save() {
    this.loading.presentLoading('Cargando...');
    if (this.agregarForm.valid) {
      console.log('REQUEST =====');
      console.log(this.agregarForm.value);
      this.gestionSvc.editarTrabajador(this.agregarForm.value).subscribe(
        (datas) => {
          console.log('RESPONSE =====');
          console.log(datas);
          if (datas.codigo !== 200) {
            this.gestionSvc.presentToast(datas.mensaje, 'danger');
          } else {
            this.gestionSvc.presentToast(datas.mensaje, 'success');
            this.closeModal({
              dni: this.agregarForm.get('dni').value,
              ...datas,
            });
          }
        },
        (err) => {
          this.modalController.dismiss();
        }
      );
      this.loading.closeLoading();
    } else {
      this.gestionSvc.presentToast('Complete todos los campos', 'danger');
      this.loading.closeLoading();
    }
  }
  closeModal(data?) {
    this.modalController.dismiss(data);
  }
}
