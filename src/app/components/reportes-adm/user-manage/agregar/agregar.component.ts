import { LoadingService } from './../../../../services/loading.service';
import { GestionUserService } from './../../../../services/gestion-user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Perfil } from 'src/app/models/Perfil.interface';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss'],
})
export class AgregarComponent implements OnInit {
  agregarForm: FormGroup;
  prefiles: Perfil[];
  inputs = [
    { title: 'DNI', nameForm: 'dni', type: 'text', maximo: 8 },
    { title: 'Nombres', nameForm: 'nombres', type: 'text', maximo: 150 },
    { title: 'Apellidos', nameForm: 'apellidos', type: 'text', maximo: 150 },
    { title: 'Teléfono', nameForm: 'telefono', type: 'text', maximo: 9 },
    { title: 'Correo', nameForm: 'correo', type: 'text', maximo: 150 },
    { title: 'Dirección', nameForm: 'direccion', type: 'text', maximo: 250 },
  ];
  constructor(
    private modalController: ModalService,
    private fb: FormBuilder,
    private gestion: GestionUserService,
    private loading: LoadingService
  ) {
    this.agregarForm = this.fb.group({
      dni: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      telefono: [
        '',
        [
          Validators.required,
          Validators.maxLength(9),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      correo: ['', [Validators.required, Validators.email]],
      direccion: ['', [Validators.required]],
      perfilId: [0, [Validators.required]],
    });
  }

  ngOnInit() {
    this.gestion.obtenerTipoTrabajador().subscribe(
      (data) => (this.prefiles = data),
      (err) => console.log(err)
    );
  }
  closeModal(data?) {
    this.modalController.dismiss(data);
  }
  save() {
    this.loading.presentLoading('Cargando...');
    if (this.agregarForm.valid) {
      console.log('REQUEST =====');
      console.log(this.agregarForm.value);
      this.gestion.agregarTrabajador(this.agregarForm.value).subscribe(
        (datas) => {
          console.log('RESPONSE =====');
          console.log(datas);
          if (datas.codigo !== 200) {
            this.gestion.presentToast(datas.mensaje, 'warning');
            this.closeModal();
          } else {
            this.gestion.presentToast(
              'Usuario registrado correctamente.',
              'success'
            );
            this.closeModal({
              dni: this.agregarForm.get('dni').value,
              ...datas,
            });
          }
        },
        (err) => {
          this.modalController.dismiss();
        }
      );
      this.loading.closeLoading();
    } else {
      this.gestion.presentToast('Complete todos los campos', 'danger');
      this.loading.closeLoading();
    }
  }
}
