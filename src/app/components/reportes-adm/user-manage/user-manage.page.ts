import { LoadingService } from './../../../services/loading.service';
import { ModalService } from './../../../services/modal.service';
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
import { GestionUserService } from './../../../services/gestion-user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Perfil } from 'src/app/models/Perfil.interface';
import { AlertController } from '@ionic/angular';
import { Subject } from 'rxjs';
import * as Responsive from 'datatables.net-responsive';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.page.html',
  styleUrls: ['./user-manage.page.scss'],
})
export class UserManagePage implements OnInit {
  manageForm: FormGroup;
  modals: HTMLIonModalElement;
  trabajadores: any;
  prefiles: Perfil[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective, { static: false })
  dtElement!: DataTableDirective;
  mostrarTabla = true;
  constructor(
    private vistasScv: GestionUserService,
    private alertController: AlertController,
    private modalSvc: ModalService,
    private loading: LoadingService
  ) {}

  ngOnInit() {
    this.vistasScv.obtenerTipoTrabajador().subscribe(
      (data) => (this.prefiles = data),
      (err) => console.log(err)
    );
    this.actualizar();
    // this.buscar();
  }
  buscar() {
    this.loading.presentLoading('Trayendo datos...');
    this.vistasScv.getListTrabajadores().subscribe(
      (data) => {
        this.cargarTabla();
        this.mostrarTabla = false;
        this.trabajadores = data;
        this.dtTrigger.next();
        this.loading.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loading.closeLoading();
      }
    );
  }

  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Gestión de Usuarios');
  }
  actualizar() {
    this.reiniciarDatosTabla();
    this.buscar();
  }

  ionViewWillLeave() {
    this.mostrarTabla = true;
    // this.modalSvc.dismiss();
  }
  cargarTabla() {
    this.dtOptions = {
      responsive: {
        responsive: true,
        details: {
          renderer: Responsive.renderer.listHiddenNodes(),
        },
      },
      columnDefs: [{ orderable: false, targets: 0 }],
      autoWidth: false,
      pagingType: 'full_numbers',
      pageLength: 10,
      info: true,
      language: {
        processing: 'Procesando...',
        search: '',
        /* lengthMenu: "Mostrar _MENU_ elementos", */
        lengthMenu: '_MENU_',
        /* info: "Mostrando desde _START_ al _END_ de _TOTAL_ elementos", */
        info: '_TOTAL_ elementos totales',
        infoEmpty: 'Ningún elemento.',
        infoFiltered: '(filtrado _MAX_ elementos total)',
        infoPostFix: '',
        loadingRecords: 'Cargando registros...',
        zeroRecords: 'No se encontraron registros',
        emptyTable: 'No se encontraron registros',
        paginate: {
          first: '⊴',
          previous: '◄',
          next: '►',
          last: '⊵',
        },
        aria: {
          sortAscending: ': Activar para ordenar la tabla en orden ascendente',
          sortDescending:
            ': Activar para ordenar la tabla en orden descendente',
        },
      },
    };
  }

  reiniciarDatosTabla() {
    if (this.dtElement !== undefined) {
      if (this.dtElement.dtInstance !== undefined) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
          this.trabajadores = [];
          this.dtOptions.responsive = {};
        });
      } else {
        this.trabajadores = [];
        this.dtOptions.responsive = {};
      }
    } else {
      this.trabajadores = [];
      this.dtOptions.responsive = {};
    }
  }

  eliminar(id: number, username: string, estado: string) {
    this.presentAlertConfirm(id, username, estado);
  }
  //ASYNCS
  async presentAlertConfirm(id, data, estado) {
    let title = 'Eliminar/';
    const botones: any[] = [
      {
        text: 'Cancelar',
        cssClass: 'botonCancelar',
        role: 'cancel',
        id: 'cancel-button',
      },
    ];
    if (estado === 'Activo') {
      botones.push({
        text: 'Deshabilitar',
        id: 'desh-button',
        cssClass: 'botonDes',
        handler: () => {
          this.mostrarTabla = true;
          this.vistasScv.deshabilitar(id);
        },
      });
      title = title + 'Deshabilitar';
    } else {
      title = title + 'Habilitar';
      botones.push({
        text: 'Habilitar',
        cssClass: 'botonHab',
        id: 'h-button',
        handler: () => {
          this.mostrarTabla = true;
          this.vistasScv.habilitar(id);
        },
      });
    }
    botones.push({
      text: 'Eliminar',
      id: 'confirm-button',
      cssClass: 'botonEli',
      handler: () => {
        this.mostrarTabla = true;
        this.vistasScv.eliminarTrabajador(id);
      },
    });
    const alert = await this.alertController.create({
      header: title,
      mode: 'ios',
      message: 'Seleccione una opción para el usuario ' + data,
      buttons: botones,
    });

    await alert.present();

    alert.onDidDismiss().then(() => {
      this.actualizar();
    });
  }
  async editar(id: number) {
    const data = await this.modalSvc.editarUsuario(id);

    if (data) {
      setTimeout(() => {
        this.actualizar();
      }, 500);
    } else {
      this.actualizar();
    }
  }
  async agregarTrabajador() {
    const data = await this.modalSvc.agregarUsuario();

    if (data) {
      setTimeout(() => {
        this.presentAlert(data);
        this.actualizar();
      }, 500);
    } else {
      this.actualizar();
    }
  }
  async presentAlert(data) {
    const alert = await this.alertController.create({
      header: 'Credenciales de nuevo usuario',
      subHeader: 'Copie las credenciales',
      message:
        'Usuario: ' + data.dni + '\r\n' + 'Contraseña: ' + data.claveTemp,
      buttons: ['OK'],
    });

    await alert.present();
  }
}
