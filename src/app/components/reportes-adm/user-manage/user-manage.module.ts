import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserManagePageRoutingModule } from './user-manage-routing.module';

import { UserManagePage } from './user-manage.page';
import { AgregarComponent } from './agregar/agregar.component';
import { DataTablesModule } from 'angular-datatables';
import { EditarComponent } from './editar/editar.component';
import { ModalService } from 'src/app/services/modal.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    DataTablesModule,
    UserManagePageRoutingModule,
  ],
  declarations: [UserManagePage, AgregarComponent, EditarComponent],
  providers: [ModalService],
})
export class UserManagePageModule {}
