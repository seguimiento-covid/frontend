import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportePersonalPage } from './reporte-personal.page';

const routes: Routes = [
  {
    path: '',
    component: ReportePersonalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportePersonalPageRoutingModule {}
