import { LoadingService } from './../../../services/loading.service';
import { Component, OnInit } from '@angular/core';
import { Personal } from 'src/app/models/Personal.interface';
import { PdfService } from 'src/app/services/pdf.service';
import { VistasService } from 'src/app/services/vistas.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-reporte-personal',
  templateUrl: './reporte-personal.page.html',
  styleUrls: ['./reporte-personal.page.scss'],
})
export class ReportePersonalPage implements OnInit {
  vista: Personal;
  date: any;

  constructor(
    private vistas: VistasService,
    private pdfSvc: PdfService,
    private navRouter: NavController,
    private loadingCtrl: LoadingService
  ) {}
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Reporte Personal');
  }
  ngOnInit() {
    if (this.vistas.getObjectSource().idUser !== '0') {
      this.mostrarDatos();
    } else {
      this.navRouter.navigateForward(['/reportesAdm/diario']);
    }
  }
  returnShowButton() {
    return this.vistas.devolverPlataforma();
  }
  ionViewDidLeave() {
    this.vistas.clearObjectSource();
  }
  async mostrarDatos() {
    this.date = this.vistas.getObjectSource();
    this.loadingCtrl.presentLoading('Cargando Datos');
    await this.vistas
      .obtenerPersonal(
        this.date.idUser,
        this.date.dia,
        this.date.mes,
        this.date.anio
      )
      .subscribe(
        (data) => {
          this.vista = data;
          this.loadingCtrl.closeLoading();
        },
        (err) => {
          console.log(err);
          this.loadingCtrl.closeLoading();
        }
      );
    this.loadingCtrl.closeLoading();
  }
  exportToPdf() {
    this.pdfSvc.exportPdfFromPersona(this.vista);
  }
}
