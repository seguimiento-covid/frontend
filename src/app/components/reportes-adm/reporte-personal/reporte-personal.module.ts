import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportePersonalPageRoutingModule } from './reporte-personal-routing.module';

import { ReportePersonalPage } from './reporte-personal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportePersonalPageRoutingModule,
  ],
  declarations: [ReportePersonalPage],
})
export class ReportePersonalPageModule {}
