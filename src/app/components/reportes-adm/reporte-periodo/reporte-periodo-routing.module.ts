import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportePeriodoPage } from './reporte-periodo.page';

const routes: Routes = [
  {
    path: '',
    component: ReportePeriodoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportePeriodoPageRoutingModule {}
