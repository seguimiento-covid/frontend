/* eslint-disable eqeqeq */
import { LoadingService } from './../../../services/loading.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Perfil } from 'src/app/models/Perfil.interface';
import { Periodo } from 'src/app/models/Periodo.interface';
import { ExcelService } from 'src/app/services/excel.service';
import { PdfService } from 'src/app/services/pdf.service';
import { VistasService } from 'src/app/services/vistas.service';

@Component({
  selector: 'app-reporte-periodo',
  templateUrl: './reporte-periodo.page.html',
  styleUrls: ['./reporte-periodo.page.scss'],
})
export class ReportePeriodoPage implements OnInit {
  periodoForm: FormGroup;
  meses: string[] = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ];
  dias = 0;
  diasTabla: number[] = [];
  trabajadores: Periodo[];
  anios: number[] = [];
  prefiles: Perfil[];
  fechaActual: Date = new Date();
  constructor(
    private fb: FormBuilder,
    private vistasScv: VistasService,
    private excelSvc: ExcelService,
    private pdfSvc: PdfService,
    private loadingSvc: LoadingService
  ) {
    this.llenarAnios();
  }
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Reporte por Periodo');
  }
  ngOnInit() {
    this.periodoForm = this.fb.group({
      mes: ['', [Validators.required]],
      anio: ['', [Validators.required]],
      param: '',
      tipoTra: '',
    });
    this.vistasScv.obtenerTipoTrabajador().subscribe(
      (data) => (this.prefiles = data),
      (err) => console.log(err)
    );
  }
  returnShowButton() {
    return this.vistasScv.devolverPlataforma();
  }
  llenarAnios() {
    for (let i = 2020; i <= this.fechaActual.getFullYear(); i++) {
      this.anios.push(i);
    }
  }

  buscarPeriodo() {
    if (this.periodoForm.valid) {
      this.loadingSvc.presentLoading('Cargando Data');
      this.periodoForm.patchValue({
        param: '',
        tipoTra: '',
      });
      this.trabajadores = undefined;
      this.llenadoTabla(
        this.periodoForm.get('mes').value,
        this.periodoForm.get('anio').value
      );
    } else {
      this.periodoForm.markAllAsTouched();
      this.vistasScv.presentToast('Complete los campos de Mes y Año', 'danger');
    }
  }
  llenadoTabla(mes, anio) {
    this.devolverDiasTotales(mes, anio);
    this.vistasScv.obtenerPeriodo(mes, anio).subscribe(
      (data) => {
        this.vistasScv.trabajadoresPeriodo = data;
        this.trabajadores = data;
        this.loadingSvc.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loadingSvc.closeLoading();
      }
    );
  }
  devolverDiasTotales(mes, anio) {
    this.dias = new Date(anio, mes, 0).getDate();
    if (
      mes != this.fechaActual.getMonth() + 1 ||
      anio != this.fechaActual.getFullYear()
    ) {
      this.diasTabla = [];
      for (let index = 1; index < this.dias + 1; index++) {
        this.diasTabla.push(index);
      }
    } else {
      this.diasTabla = [];
      for (let index = 1; index <= this.fechaActual.getDate(); index++) {
        this.diasTabla.push(index);
      }
    }
  }
  getTrabajadores() {
    if (this.trabajadores === undefined) {
      return true;
    }
    this.trabajadores = [];
    let parame = ' ';
    if (this.periodoForm.get('param').value !== '') {
      parame = this.periodoForm.get('param').value;
    }
    const tipoTra: string = this.periodoForm.get('tipoTra').value;
    this.vistasScv.trabajadoresPeriodo.forEach((element) => {
      if (
        (element.nombre.toUpperCase().includes(parame.toUpperCase()) ||
          element.dni.includes(parame)) &&
        element.perfil.toUpperCase().includes(tipoTra.toUpperCase())
      ) {
        this.trabajadores.push(element);
      }
    });
  }
  exportToExcel() {
    this.excelSvc.exportToExcelPersonal(
      this.trabajadores,
      this.diasTabla,
      this.periodoForm.get('mes').value,
      this.periodoForm.get('anio').value
    );
  }
  exportPdfPersonal() {
    this.pdfSvc.exportPdfPersonal(
      this.trabajadores,
      this.diasTabla,
      this.periodoForm.get('mes').value,
      this.periodoForm.get('anio').value
    );
  }
}
