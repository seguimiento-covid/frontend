import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportePeriodoPageRoutingModule } from './reporte-periodo-routing.module';

import { ReportePeriodoPage } from './reporte-periodo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ReportePeriodoPageRoutingModule,
  ],
  declarations: [ReportePeriodoPage],
})
export class ReportePeriodoPageModule {}
