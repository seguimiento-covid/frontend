import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportesAdmPageRoutingModule } from './reportes-adm-routing.module';

import { ReportesAdmPage } from './reportes-adm.page';
import { PagePartModule } from '../page-part/page-part.module';
import { VistasService } from 'src/app/services/vistas.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportesAdmPageRoutingModule,
    PagePartModule,
  ],
  declarations: [ReportesAdmPage],
  providers: [VistasService],
})
export class ReportesAdmPageModule {}
