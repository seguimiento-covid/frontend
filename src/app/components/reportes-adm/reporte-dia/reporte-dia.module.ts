import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReporteDiaPageRoutingModule } from './reporte-dia-routing.module';

import { ReporteDiaPage } from './reporte-dia.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ReporteDiaPageRoutingModule,
  ],
  declarations: [ReporteDiaPage],
})
export class ReporteDiaPageModule {}
