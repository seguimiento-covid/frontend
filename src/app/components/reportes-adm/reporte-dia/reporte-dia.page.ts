import { LoadingService } from './../../../services/loading.service';
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Diario } from 'src/app/models/Diario.interface';
import { ExcelService } from 'src/app/services/excel.service';
import { PdfService } from 'src/app/services/pdf.service';
import { VistasService } from 'src/app/services/vistas.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-reporte-dia',
  templateUrl: './reporte-dia.page.html',
  styleUrls: ['./reporte-dia.page.scss'],
})
export class ReporteDiaPage implements OnInit {
  fechaActual: Date = new Date();
  data: Diario[] = [];
  diaForm: FormGroup;
  arrFecha: string[] = [];
  fechaConsul: Date = new Date();

  constructor(
    private vistaScv: VistasService,
    private fb: FormBuilder,
    private router: Router,
    private excelSvc: ExcelService,
    private pdfSvc: PdfService,
    private loadingCtrl: LoadingService,
    private token: TokenService
  ) {
    this.diaForm = this.fb.group({
      dia: ['', [Validators.required]],
      param: '',
    });
  }
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Reporte por Día');
  }
  ngOnInit() {
    this.arrFecha.push(`${this.fechaActual.getFullYear()}`);
    this.arrFecha.push(`${this.fechaActual.getMonth() + 1}`);
    this.arrFecha.push(`${this.fechaActual.getDate()}`);
    this.vistaScv
      .obtenerDiario(
        '' + this.fechaActual.getDate(),
        '' + (this.fechaActual.getMonth() + 1),
        '' + this.fechaActual.getFullYear()
      )
      .subscribe(
        (data) => {
          this.data = data;
          this.vistaScv.trabajadoresDiario = data;
          this.saveDate(this.token.getUserID());
        },
        (err) => {
          console.log(err);
        }
      );
  }
  returnShowButton() {
    return this.vistaScv.devolverPlataforma();
  }
  buscarPeriodo() {
    this.loadingCtrl.presentLoading('Cargando data');
    this.arrFecha = this.diaForm.get('dia').value.split('-');
    if (this.diaForm.valid) {
      this.vistaScv
        .obtenerDiario(this.arrFecha[2], this.arrFecha[1], this.arrFecha[0])
        .subscribe(
          (data) => {
            this.data = data;
            this.vistaScv.trabajadoresDiario = data;
            this.fechaConsul = this.diaForm.get('dia').value;
            this.saveDate(this.token.getUserID());
            this.loadingCtrl.closeLoading();
          },
          (err) => {
            console.log(err);
            this.loadingCtrl.closeLoading();
          }
        );
    } else {
      this.diaForm.markAllAsTouched();
      this.loadingCtrl.closeLoading();
      this.vistaScv.presentToast('Complete el campo de Fecha', 'danger');
    }
  }

  goReceiver(id) {
    if (this.diaForm.get('dia').value !== '') {
      const arrFecha = this.diaForm.get('dia').value.split('-');
      this.vistaScv.sendObjectSource({
        idUser: id,
        dia: arrFecha[2],
        mes: arrFecha[1],
        anio: arrFecha[0],
      });
    } else {
      this.vistaScv.sendObjectSource({
        idUser: id,
        dia: this.fechaActual.getDate(),
        mes: this.fechaActual.getMonth() + 1,
        anio: this.fechaActual.getFullYear(),
      });
    }
    this.router.navigate(['/control/personal']);
  }
  saveDate(id) {
    if (this.diaForm.get('dia').value !== '') {
      const arrFecha = this.diaForm.get('dia').value.split('-');
      this.vistaScv.sendObjectSource({
        idUser: id,
        dia: arrFecha[2],
        mes: arrFecha[1],
        anio: arrFecha[0],
      });
    } else {
      this.vistaScv.sendObjectSource({
        idUser: id,
        dia: this.fechaActual.getDate(),
        mes: this.fechaActual.getMonth() + 1,
        anio: this.fechaActual.getFullYear(),
      });
    }
  }
  getTrabajadores() {
    this.data = [];
    const param = this.diaForm.get('param').value;
    this.vistaScv.trabajadoresDiario.forEach((element) => {
      if (
        element.nombre.toUpperCase().includes(param.toUpperCase()) ||
        element.dni.includes(param)
      ) {
        this.data.push(element);
      }
    });
  }

  async exportToExcel() {
    const dia = window.localStorage.getItem('Dia');
    const mes = window.localStorage.getItem('Mes');
    const anio = window.localStorage.getItem('Anio');

    this.loadingCtrl.presentLoading('Exportando Reporte');
    this.vistaScv
      .retornarExcel(this.arrFecha[2], this.arrFecha[1], this.arrFecha[0])
      .subscribe(
        (data) => {
          this.excelSvc.exportToExcelDia(data, this.data, dia, mes, anio);
        },
        (err) => {
          console.log(err);
        }
      );

    this.loadingCtrl.closeLoading();
  }

  exportToPdf() {
    if (this.diaForm.get('dia').value === '') {
      this.pdfSvc.exportPdfDiario(
        this.data,
        this.fechaActual.getDate(),
        this.fechaActual.getMonth() + 1,
        this.fechaActual.getFullYear(),
        this.token.getDni()
      );
    } else {
      const arr = this.diaForm.get('dia').value.split('-');
      this.pdfSvc.exportPdfDiario(
        this.data,
        arr[2],
        arr[1],
        arr[0],
        this.token.getDni()
      );
    }
  }
  returnColor(estado: number) {
    if (estado === 2) {
      return 'danger';
    } else if (estado === 1) {
      return 'warning';
    } else if (estado === 3) {
      return 'success';
    } else {
      return 'light';
    }
  }
  sendPush() {
    const appIds: string[] = this.data
      .filter(
        (element) =>
          element.appId !== undefined &&
          element.appId !== '' &&
          element.estado === 0
      )
      .map((ele) => ele.appId);
    this.vistaScv.sendPush(appIds);
  }
}
