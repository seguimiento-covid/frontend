import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReporteDiaPage } from './reporte-dia.page';

const routes: Routes = [
  {
    path: '',
    component: ReporteDiaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReporteDiaPageRoutingModule {}
