import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuardGuard } from 'src/app/guards/admin-guard.guard';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { ReportesAdmPage } from './reportes-adm.page';

const routes: Routes = [
  {
    path: '',
    component: ReportesAdmPage,
    children: [
      {
        path: 'menuCovid',
        loadChildren: () =>
          import('../../components/reporte-covid/reporte-covid.module').then(
            (m) => m.ReporteCovidPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'menu',
        loadChildren: () =>
          import('../../components/reporte/reporte.module').then(
            (m) => m.ReportePageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'principal',
        loadChildren: () =>
          import('../../components/principal/principal.module').then(
            (m) => m.PrincipalPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: 'periodo',
        loadChildren: () =>
          import('./reporte-periodo/reporte-periodo.module').then(
            (m) => m.ReportePeriodoPageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'diario',
        loadChildren: () =>
          import('./reporte-dia/reporte-dia.module').then(
            (m) => m.ReporteDiaPageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'personal',
        loadChildren: () =>
          import('./reporte-personal/reporte-personal.module').then(
            (m) => m.ReportePersonalPageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'asignarForm',
        loadChildren: () =>
          import('./asignar-formulario/asignar-formulario.module').then(
            (m) => m.AsignarFormularioPageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'rango',
        loadChildren: () =>
          import('./reporte-rango/reporte-rango.module').then(
            (m) => m.ReporteRangoPageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'gestionUsuario',
        loadChildren: () =>
          import('./user-manage/user-manage.module').then(
            (m) => m.UserManagePageModule
          ),
        canActivate: [AdminGuardGuard],
        data: { expectedRol: ['Administrador'] },
      },
      {
        path: 'perfil/:accion',
        loadChildren: () =>
          import('../../components/perfil/perfil.module').then(
            (m) => m.PerfilPageModule
          ),
        canActivate: [AuthGuard],
      },
      {
        path: '',
        redirectTo: '/control/principal',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportesAdmPageRoutingModule {}
