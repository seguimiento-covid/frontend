import { LoadingService } from './../../../services/loading.service';
import { ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Asignacion } from 'src/app/models/Asignacion.interface';
import { VistasService } from 'src/app/services/vistas.service';

@Component({
  selector: 'app-asignar-formulario',
  templateUrl: './asignar-formulario.page.html',
  styleUrls: ['./asignar-formulario.page.scss'],
})
export class AsignarFormularioPage implements OnInit {
  usuarios: Asignacion[] = [];
  param = '';
  constructor(
    private reporteSvc: VistasService,
    private toast: ToastController,
    private loadingCtrl: LoadingService
  ) {}
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Asignar Formulario');
  }
  ngOnInit() {}
  ionViewDidEnter() {
    this.loadingCtrl.presentLoading('Cargandi Data');
    this.reporteSvc.obtenerReportesHasCovid().subscribe(
      (data) => {
        this.usuarios = data;
        this.reporteSvc.asignadores = data;
        this.loadingCtrl.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loadingCtrl.closeLoading();
      }
    );
  }
  async cambiarEstado(id: number, hasCovid: boolean) {
    this.loadingCtrl.presentLoading('Actualizando Estado');
    await this.reporteSvc.cambiarEstatus({ id, hasCovid: !hasCovid }).subscribe(
      (data) => {
        this.usuarios.forEach((element) => {
          if (element.id === id) {
            element.hasCovid = !element.hasCovid;
          }
        });
        this.loadingCtrl.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loadingCtrl.closeLoading();
      }
    );
  }
  getTrabajadores() {
    this.usuarios = [];
    const param = this.param;
    this.reporteSvc.asignadores.forEach((element) => {
      if (
        element.nombre.toUpperCase().includes(param.toUpperCase()) ||
        element.dni.includes(param)
      ) {
        this.usuarios.push(element);
      }
    });
  }

  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
