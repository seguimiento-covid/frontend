import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsignarFormularioPageRoutingModule } from './asignar-formulario-routing.module';

import { AsignarFormularioPage } from './asignar-formulario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsignarFormularioPageRoutingModule
  ],
  declarations: [AsignarFormularioPage]
})
export class AsignarFormularioPageModule {}
