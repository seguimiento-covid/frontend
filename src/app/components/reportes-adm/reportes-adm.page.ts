import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-reportes-adm',
  templateUrl: './reportes-adm.page.html',
  styleUrls: ['./reportes-adm.page.scss'],
})
export class ReportesAdmPage implements OnInit {
  rutas = [
    {
      ruta: '/control/principal',
      nombre: 'Principal',
      icon: 'document-outline',
      children: [],
      rol: '',
    },
    {
      nombre: 'Formulario',
      ruta: '/control/menu',
      icon: 'document-lock-outline',
      children: [],
      rol: '',
    },
    {
      nombre: 'Formulario',
      ruta: '/control/menuCovid',
      icon: 'document-lock-outline',
      children: [],
      rol: '',
    },
    {
      nombre: 'Perfil',
      icon: 'person-outline',
      open: false,
      children: [
        {
          nombre: 'Ver mis Datos',
          ruta: '/control/perfil/ver',
          icon: 'search-circle-outline',
        },
        {
          nombre: 'Editar mis datos',
          ruta: '/control/perfil/editar',
          icon: 'create-outline',
        },
        {
          nombre: 'Cambiar contraseña',
          ruta: '/control/perfil/changePass',
          icon: 'bag-outline',
        },
      ],
      rol: '',
    },
    {
      ruta: '/control/periodo',
      nombre: 'Reporte por Periodo',
      icon: 'calendar-clear-outline',
      children: [],
      rol: 'Administrador',
    },
    {
      ruta: '/control/diario',
      nombre: 'Reporte por Día',
      icon: 'calendar-number-outline',
      children: [],
      rol: 'Administrador',
    },
    {
      ruta: '/control/rango',
      nombre: 'Reporte por Rango',
      icon: 'calendar-outline',
      children: [],
      rol: 'Administrador',
    },
    {
      ruta: '/control/asignarForm',
      nombre: 'Asignar Formulario',
      icon: 'toggle-outline',
      children: [],
      rol: 'Administrador',
    },
    {
      ruta: '/control/gestionUsuario',
      nombre: 'Gestión usuario',
      icon: 'people-outline',
      children: [],
      rol: 'Administrador',
    },
  ];
  constructor(private menu: MenuController, private token: TokenService) {}

  ngOnInit() {}
  openMenu() {
    this.menu.open();
  }
  closeMenu() {
    this.menu.close();
  }
  obtenerMensaje() {
    return window.localStorage.getItem('Menu');
  }
  retonarMenus() {
    if (this.token.getAuthorities() !== 'Administrador') {
      return this.subRetornoMenu().filter(
        (element) => element.rol !== 'Administrador'
      );
    } else {
      return this.subRetornoMenu();
    }
  }
  subRetornoMenu() {
    const arr = this.rutas;
    let arrFinal = [];
    if (!this.verificarCompletado()) {
      if (this.verificarCovid()) {
        arrFinal = arr.filter((element) => element.ruta !== '/control/menu');
      } else {
        arrFinal = arr.filter(
          (element) => element.ruta !== '/control/menuCovid'
        );
      }
    } else {
      arrFinal = arr.filter(
        (element) =>
          element.ruta !== '/control/menuCovid' &&
          element.ruta !== '/control/menu'
      );
    }
    return arrFinal;
  }
  verificarCompletado(): boolean {
    if (this.token.getCompleted() !== 'no') {
      return true;
    } else {
      return false;
    }
  }

  verificarCovid(): boolean {
    if (this.token.getHasCovid() !== 'no') {
      return true;
    } else {
      return false;
    }
  }
}
