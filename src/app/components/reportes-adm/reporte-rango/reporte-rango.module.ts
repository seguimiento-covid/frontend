import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReporteRangoPageRoutingModule } from './reporte-rango-routing.module';

import { ReporteRangoPage } from './reporte-rango.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ReporteRangoPageRoutingModule,
  ],
  declarations: [ReporteRangoPage],
})
export class ReporteRangoPageModule {}
