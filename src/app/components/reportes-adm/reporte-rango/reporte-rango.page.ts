import { LoadingService } from './../../../services/loading.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Perfil } from 'src/app/models/Perfil.interface';
import { Periodo } from 'src/app/models/Periodo.interface';
import { VistasService } from 'src/app/services/vistas.service';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-reporte-rango',
  templateUrl: './reporte-rango.page.html',
  styleUrls: ['./reporte-rango.page.scss'],
})
export class ReporteRangoPage implements OnInit {
  rangoForm: FormGroup;
  dias = 0;
  diasTabla: string[] = [];
  trabajadores: Periodo[];
  prefiles: Perfil[];
  fechaActual: Date = new Date();

  constructor(
    private fb: FormBuilder,
    private vistasScv: VistasService,
    private loadingSvc: LoadingService,
    private excelSvc: ExcelService
  ) {
    this.rangoForm = this.fb.group({
      desde: ['', [Validators.required]],
      hasta: ['', [Validators.required]],
      param: [''],
      tipoTra: [''],
    });
  }
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Reporte por Rango');
  }
  ngOnInit() {
    this.vistasScv.obtenerTipoTrabajador().subscribe(
      (data) => (this.prefiles = data),
      (err) => console.log(err)
    );
  }
  returnShowButton() {
    return this.vistasScv.devolverPlataforma();
  }
  buscarRangos() {
    if (this.rangoForm.valid) {
      this.loadingSvc.presentLoading('Cargando Data');
      this.rangoForm.patchValue({
        param: '',
        tipoTra: '',
      });
      if (this.validacionFechas() > 31) {
        this.vistasScv.presentToast(
          'Está buscando demasiados días. Máximo permitido 31 días.',
          'warning'
        );
        this.loadingSvc.closeLoading();
        return true;
      }
      this.trabajadores = undefined;
      this.llenadoTabla(
        this.rangoForm.get('desde').value,
        this.rangoForm.get('hasta').value
      );
    } else {
      this.rangoForm.markAllAsTouched();
      this.vistasScv.presentToast(
        'Complete los campos de Día Inicial y Final',
        'danger'
      );
    }
  }
  validacionFechas() {
    const MILISENGUNDOS_POR_DIA = 1000 * 60 * 60 * 24;
    const desde = new Date(this.rangoForm.get('desde').value).getTime();
    const hasta = new Date(this.rangoForm.get('hasta').value).getTime();
    const dif = Math.floor((hasta - desde) / MILISENGUNDOS_POR_DIA);
    return dif;
  }
  llenadoTabla(desde, hasta) {
    this.asigDiasTabla(desde, hasta);
    this.vistasScv.obtenerRangos(desde, hasta).subscribe(
      (data) => {
        this.vistasScv.trabajadoresRango = data;
        this.trabajadores = data;
        this.loadingSvc.closeLoading();
      },
      (err) => {
        console.log(err);
        this.loadingSvc.closeLoading();
      }
    );
  }
  asigDiasTabla(desde, hasta) {
    const fechaI: Date = new Date(desde);
    const fechaF: Date = new Date(hasta);
    this.diasTabla = [];
    const dif = Math.round(
      (fechaF.getTime() - fechaI.getTime()) / (1000 * 60 * 60 * 24)
    );
    for (let i = 0; i <= dif; i++) {
      fechaI.setDate(fechaI.getDate() + 1);
      this.diasTabla.push(fechaI.getDate() + '-' + (fechaI.getMonth() + 1));
    }
  }
  getTrabajadores() {
    if (this.trabajadores === undefined) {
      return true;
    }
    this.trabajadores = [];
    let parame = ' ';
    if (this.rangoForm.get('param').value !== '') {
      parame = this.rangoForm.get('param').value;
    }
    const tipoTra: string = this.rangoForm.get('tipoTra').value;
    this.vistasScv.trabajadoresRango.forEach((element) => {
      if (
        (element.nombre.toUpperCase().includes(parame.toUpperCase()) ||
          element.dni.includes(parame)) &&
        element.perfil.toUpperCase().includes(tipoTra.toUpperCase())
      ) {
        this.trabajadores.push(element);
      }
    });
  }
  exportToExcel() {
    this.excelSvc.exportToExcelRango(
      this.trabajadores,
      this.diasTabla,
      this.rangoForm.get('desde').value,
      this.rangoForm.get('hasta').value
    );
  }
}
