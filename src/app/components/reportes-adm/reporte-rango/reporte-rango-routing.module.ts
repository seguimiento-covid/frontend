import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReporteRangoPage } from './reporte-rango.page';

const routes: Routes = [
  {
    path: '',
    component: ReporteRangoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReporteRangoPageRoutingModule {}
