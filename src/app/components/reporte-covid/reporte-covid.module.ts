import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReporteCovidPageRoutingModule } from './reporte-covid-routing.module';

import { ReporteCovidPage } from './reporte-covid.page';
import { PagePartModule } from '../page-part/page-part.module';
import { ReporteService } from 'src/app/services/reporte.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ReporteCovidPageRoutingModule,
    PagePartModule,
  ],
  declarations: [ReporteCovidPage],
  providers: [ReporteService],
})
export class ReporteCovidPageModule {}
