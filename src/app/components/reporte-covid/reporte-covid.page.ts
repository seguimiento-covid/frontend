import { LoadingService } from './../../services/loading.service';
import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { Signos } from 'src/app/models/signos.interface';
import { Sintomas } from 'src/app/models/Sintomas.interface';
import { ReporteService } from 'src/app/services/reporte.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-reporte-covid',
  templateUrl: './reporte-covid.page.html',
  styleUrls: ['./reporte-covid.page.scss'],
})
export class ReporteCovidPage implements OnInit {
  myVar: Date = new Date();
  sintomas: Sintomas[];
  sintomasFinal: Sintomas[] = [];
  signos: Signos[];
  signosFinal: Signos[] = [];
  reporteForm: FormGroup;
  userId: string;
  // numValidator = ;
  constructor(
    private reporteSvc: ReporteService,
    private token: TokenService,
    private fb: FormBuilder,
    private toast: ToastController,
    private loadingSvc: LoadingService
  ) {
    this.loadingSvc.presentLoading('Cargando Reporte');
    this.userId = this.token.getUserID();
    this.reporteForm = this.fb.group({
      sintomasss: this.fb.array([], [Validators.required]),
      temp: [
        30,
        [
          Validators.required,
          Validators.pattern(/^\d*\.?\d{0,2}$/g),
          Validators.min(30),
          Validators.max(42),
        ],
      ],
      comentario: [''],
      signosss: this.fb.array([], [Validators.required]),
      anotherSig: [''],
      anotherSint: [''],
    });
    this.reporteSvc.obtenerSignos().subscribe(
      (data) => (this.signos = data),
      (error) => console.log(error)
    );
    this.reporteSvc.obtenerSintomas().subscribe(
      (data) => {
        this.sintomas = data;
        this.loadingSvc.closeLoading();
      },

      (err) => console.log(err)
    );
  }

  ngOnInit() {}
  ionViewWillEnter() {
    window.localStorage.setItem('Menu', 'Formulario COVID');
  }
  //Validadores de Sintomas
  devolverSintomas(): Sintomas[] {
    this.sintomasFinal = [];
    this.sintomasFinal = this.reporteForm
      .get('sintomasss')
      .value.map((element) => {
        if (element === 14) {
          return {
            sintomaId: 14,
            otroDetalle: this.reporteForm.get('anotherSint').value,
          };
        } else {
          return { sintomaId: element, otroDetalle: '' };
        }
      });
    return this.sintomasFinal;
  }
  //Validacion de Signos
  devolverSignos(): Signos[] {
    this.signosFinal = [];
    this.signosFinal = this.reporteForm.get('signosss').value.map((element) => {
      if (element === 6) {
        return {
          signoId: 6,
          otroDetalle: this.reporteForm.get('anotherSig').value,
        };
      } else {
        return { signoId: element, otroDetalle: '' };
      }
    });
    return this.signosFinal;
  }
  //Evento Change Sintomas
  onChangeBoxSintomas(e) {
    const isArray: FormArray = this.reporteForm.get('sintomasss') as FormArray;
    if (e.target.checked) {
      isArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      isArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          isArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  //Evento Change Signos
  onChangeBoxSignos(e) {
    const isArray: FormArray = this.reporteForm.get('signosss') as FormArray;
    if (e.target.checked) {
      isArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      isArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          isArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
  save() {
    if (this.reporteForm.valid) {
      this.token.setCompleted('true');
      this.reporteSvc.enviarReporteCovid({
        id: 0,
        userId: Number(this.userId),
        temperatura: this.reporteForm.get('temp').value,
        comentario: this.reporteForm.get('comentario').value,
        sintomas: this.devolverSintomas(),
        signos: this.devolverSignos(),
        fecha: this.myVar.toJSON(),
      });
    } else {
      this.reporteForm.markAllAsTouched();
      this.presentToast('Por favor complete todos los campos!', 'warning');
    }
  }
  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'top',
      duration: 4000,
    });
    toast.present();
  }
}
