import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReporteCovidPage } from './reporte-covid.page';

const routes: Routes = [
  {
    path: '',
    component: ReporteCovidPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReporteCovidPageRoutingModule {}
