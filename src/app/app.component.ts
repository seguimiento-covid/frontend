import { PushService } from './services/push.service';
import { SessionService } from './services/session.service';
import { Component } from '@angular/core';
import { VersionService } from './services/version.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private sessionSvc: SessionService,
    private versionSvc: VersionService,
    private pushSvc: PushService
  ) {
    this.versionSvc.obtenerUltimaVersion();
    this.pushSvc.oneSignalInit();
    this.sessionSvc.init();
  }
}
