import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { Drivers } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReportesAdmPageModule } from './components/reportes-adm/reportes-adm.module';
import { HttpConfigInterceptor } from './interceptors/prod-interceptor.service';
import { DatePipe } from '@angular/common';
import { NotFoundModule } from './components/not-found/not-found.module';
import { IonicStorageModule } from '@ionic/storage-angular';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ReportesAdmPageModule,
    NotFoundModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: [
        Drivers.SecureStorage,
        Drivers.IndexedDB,
        Drivers.LocalStorage,
      ],
    }),
  ],
  providers: [
    OneSignal,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    DatePipe,
    FileOpener,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
