import { LoadingService } from './loading.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { environment } from 'src/environments/environment';
import { Auth } from '../models/auth.interface';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private storages: Storage | null = null;
  constructor(
    private storage: Storage,
    private http: HttpClient,
    private token: TokenService,
    private loadingSvc: LoadingService
  ) {}
  public async init() {
    const storage = await this.storage.create();
    this.storages = storage;
  }

  public set(key: string, value: string) {
    this.storages?.set(key, value);
  }
  public get(key: string) {
    return this.storages?.get(key);
  }
  public clearAll() {
    this.storages?.clear();
  }
  refreshToken() {
    this.loadingSvc.presentLoading('Iniciando Sesión');
    this.get('dni')
      .then((dni) => {
        this.get('pass').then((password) => {
          this.http
            .post<Auth>(environment.formularioEnvioUrl + 'session', {
              dni,
              password,
            })
            .subscribe(
              async (data) => {
                this.token.setDni(data.nombres);
                this.loadingSvc.closeLoading();
                this.token.setAuthorities(data.perfil);
                this.token.setToken(data.token);
                this.token.setUserID('' + data.id);
                this.token.setCompleted(data.completado ? 'si' : 'no');
                await this.token.setHasCovid(data.hasCovid ? 'si' : 'no');
              },
              (err) => {
                this.loadingSvc.closeLoading();
                console.log(err);
              }
            );
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
