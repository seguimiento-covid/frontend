import { Signos } from 'src/app/models/signos.interface';
import { environment } from 'src/environments/environment';
import { Reporte } from 'src/app/models/Reporte.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sintomas } from 'src/app/models/Sintomas.interface';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ReportCovid } from '../models/ReportCovid.interface';
import { LoadingService } from './loading.service';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'any',
})
export class ReporteService {
  userId: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private toast: ToastController,
    private loadingSvc: LoadingService,
    private tokenSvc: TokenService
  ) {}

  public enviarReporte(report: Reporte) {
    this.loadingSvc.presentLoading('Enviando reporte');
    this.http
      .post<boolean>(environment.formularioEnvioUrl + 'reporte', report)
      .subscribe(
        (bool) => {
          this.loadingSvc.closeLoading();
          if (bool) {
            this.presentToast('Formulario Completado correctamente', 'success');
            this.router.navigate(['']);
          } else {
            this.presentToast(
              'Formulario Completado incorrectamente',
              'warning'
            );
          }
        },
        (err) => {
          this.presentToast(err.message, 'danger');
          this.loadingSvc.closeLoading();
        }
      );
  }
  public enviarReporteCovid(report: ReportCovid) {
    this.loadingSvc.presentLoading('Enviando reporte');
    this.http
      .post<boolean>(`${environment.formularioEnvioUrl}reporte/covid`, report)
      .subscribe(
        (response) => {
          this.loadingSvc.closeLoading();
          if (response) {
            this.presentToast('Formulario Completado correctamente', 'success');
            this.router.navigate(['']);
          } else {
            this.presentToast(
              'Formulario Completado incorrectamente',
              'warning'
            );
          }
        },
        (err) => {
          console.log(err);
          this.loadingSvc.closeLoading();
        }
      );
  }

  public obtenerSintomas(): Observable<Sintomas[]> {
    this.userId = this.tokenSvc.getUserID();
    return this.http.get<Sintomas[]>(
      environment.formularioEnvioUrl + 'reporte/sintomas/' + this.userId
    );
  }
  public obtenerSignos(): Observable<Signos[]> {
    this.userId = this.tokenSvc.getUserID();
    return this.http.get<Signos[]>(
      environment.formularioEnvioUrl + 'reporte/signos/' + this.userId
    );
  }
  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
