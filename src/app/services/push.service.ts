import { Injectable } from '@angular/core';
import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PushService {
  userId: string;

  constructor(private oneSignal: OneSignal) {}
  oneSignalInit() {
    this.oneSignal.startInit(environment.oneAppPushId, environment.remitente);

    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.InAppAlert
    );

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      console.log('Notificación Abierta');
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      console.log('Notificación Cerrada');
    });

    this.oneSignal
      .getIds()
      .then((info) => {
        window.localStorage.setItem('uidOne', info.userId);
      })
      .catch((ex) => window.localStorage.setItem('uidOne', ex));

    this.oneSignal.endInit();
  }
}
