import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AgregarComponent } from '../components/reportes-adm/user-manage/agregar/agregar.component';
import { EditarComponent } from '../components/reportes-adm/user-manage/editar/editar.component';

@Injectable({
  providedIn: 'any',
})
export class ModalService {
  modals: HTMLIonModalElement;
  constructor(private modalController: ModalController) {}
  async agregarUsuario() {
    this.modals = await this.modalController.create({
      component: AgregarComponent,
      swipeToClose: false,
      backdropDismiss: false,
    });

    await this.modals.present();
    const { data } = await this.modals.onDidDismiss();
    return data;
  }
  async dismiss(data?) {
    this.modalController.dismiss(data);
  }
  async editarUsuario(id) {
    this.modals = await this.modalController.create({
      component: EditarComponent,
      componentProps: { id },
      swipeToClose: false,
      backdropDismiss: false,
    });
    await this.modals.present();

    const { data } = await this.modals.onDidDismiss();
    return data;
  }
}
