import { SessionService } from './session.service';
import { LoadingService } from './loading.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import {
  Auth,
  RequestChangeClave,
  Respuesta,
} from 'src/app/models/auth.interface';
import { environment } from 'src/environments/environment';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private toast: ToastController,
    private token: TokenService,
    private loadingSvc: LoadingService,
    private session: SessionService
  ) {}

  public login(usuario: Auth) {
    this.loadingSvc.presentLoading('Autenticando');
    this.http
      .post<Auth>(environment.formularioEnvioUrl + 'session', usuario)
      .subscribe(
        (data) => {
          if (!data.logueado) {
            this.presentToast('DNI o contraseña inválidos', 'danger');
            this.loadingSvc.closeLoading();
          } else {
            if (data.estado === 0) {
              this.token.setAuthorities(data.perfil);
              this.token.setDni(data.nombres);
              this.token.setToken(data.token);
              this.token.setUserID('' + data.id);
              this.token.setCompleted(data.completado ? 'si' : 'no');
              this.token.setHasCovid(data.hasCovid ? 'si' : 'no');
              this.token.setCodigo(data.codigo);
              if (data.codigo === 200 && usuario.recordar) {
                this.session.set('dni', usuario.dni);
                this.session.set('token', data.token);
                this.session.set('pass', usuario.password);
              }
              if (
                window.localStorage.getItem('uidOne') !==
                  'cordova_not_available' &&
                window.localStorage.getItem('uidOne') !== null
              ) {
                this.actualizarIDApp(
                  data.id,
                  window.localStorage.getItem('uidOne')
                );
              }
              this.loadingSvc.closeLoading();
              if (data.codigo === 200) {
                this.router.navigate(['/control/principal']);
              } else if (data.codigo === 205) {
                this.router.navigate(['/cambiarClave']);
              }
            } else if (data.estado === 2) {
              this.presentToast(
                'Usuario Deshabilitado, contacte a su administrador.',
                'warning'
              );
              this.loadingSvc.closeLoading();
            }
          }
        },
        (err) => {
          this.loadingSvc.closeLoading();
          console.log(err);
        }
      );
  }
  logout() {
    this.token.logOut();
    this.session.clearAll();
    this.router.navigate(['/login']);
  }
  actualizarIDApp(id, appId) {
    this.http
      .get<boolean>(`${environment.formularioEnvioUrl}push/${id}/${appId}`)
      .subscribe((data) => data);
  }
  changePass(id, claveAnterior, claveNueva) {
    this.loadingSvc.presentLoading('Validando credenciales...');
    this.http
      .post<Respuesta>(
        `${environment.formularioEnvioUrl}session/editarContra`,
        { id, claveAnterior, claveNueva }
      )
      .subscribe(
        (data) => {
          if (data.codigo === 200) {
            this.presentToast(data.mensaje, 'success');
            this.router.navigate(['/login']);
            this.token.setCodigo('' + 200);
            this.loadingSvc.closeLoading();
          } else {
            this.presentToast(data.mensaje, 'danger');
            this.loadingSvc.closeLoading();
          }
        },
        (err) => {
          this.loadingSvc.closeLoading();
        }
      );
  }
  async presentToast(mensaje, status) {
    const toast = await this.toast.create({
      message: mensaje,
      color: status,
      position: 'top',
      duration: 2000,
    });
    toast.present();
  }
}
