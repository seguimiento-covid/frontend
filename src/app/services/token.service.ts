import { Injectable } from '@angular/core';
import { EncrypterService } from './encrypter.service';

const TOKEN_KEY = 'AuthToken';
const USERNAME_KEY = 'AuthUserName';
const AUTHORITIES_KEY = 'AuthAuthorities';
const ID_KEY = 'AuthIdUser';
const COMPLETED_FORM = 'AuthComplete';
const HAS_COVID = 'AuthHasCovid';
@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor(private encrp: EncrypterService) {}

  public setToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, this.encrp.encrypt(token));
  }

  public getToken(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(TOKEN_KEY));
  }

  public setDni(nombre: string): void {
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY, this.encrp.encrypt(nombre));
  }

  public getDni(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(USERNAME_KEY));
  }

  public setAuthorities(authorities: string): void {
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(
      AUTHORITIES_KEY,
      this.encrp.encrypt(authorities)
    );
  }

  public getAuthorities(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(AUTHORITIES_KEY));
  }

  public setUserID(authorities): void {
    window.sessionStorage.removeItem(ID_KEY);
    window.sessionStorage.setItem(ID_KEY, this.encrp.encrypt(authorities));
  }

  public getUserID(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(ID_KEY));
  }

  public setCompleted(authorities): void {
    window.sessionStorage.removeItem(COMPLETED_FORM);
    window.sessionStorage.setItem(
      COMPLETED_FORM,
      this.encrp.encrypt(authorities)
    );
  }

  public getCompleted(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(COMPLETED_FORM));
  }

  public setHasCovid(authorities): void {
    window.sessionStorage.removeItem(HAS_COVID);
    window.sessionStorage.setItem(HAS_COVID, this.encrp.encrypt(authorities));
  }

  public getHasCovid(): string {
    return this.encrp.desencrypt(sessionStorage.getItem(HAS_COVID));
  }
  public setCodigo(codigo) {
    window.sessionStorage.removeItem('Codigo');
    window.sessionStorage.setItem('Codigo', this.encrp.encrypt('' + codigo));
  }
  public getCodigo(): string {
    return this.encrp.desencrypt(sessionStorage.getItem('Codigo'));
  }
  public logOut(): void {
    window.sessionStorage.clear();
    window.localStorage.clear();
  }
}
