import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'any',
})
export class VersionService {
  constructor(private http: HttpClient, private toast: ToastController) {}
  async obtenerUltimaVersion() {
    await this.http
      .get<string>(environment.formularioEnvioUrl + 'session/version')
      .subscribe(
        async (data) => {
          if (data !== environment.version) {
            const toast = await this.toast.create({
              message: 'Debe actualizar la aplicación',
              color: 'warning',
              position: 'middle',
              buttons: [
                {
                  text: 'Actualizar',
                  role: 'cancel',
                  handler: () => {
                    window.location.reload();
                  },
                },
              ],
            });
            toast.present();
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }
}
