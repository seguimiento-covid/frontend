import { FileSaveService } from './file-save.service';
/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { Periodo } from '../models/Periodo.interface';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { Personal } from '../models/Personal.interface';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root',
})
export class PdfService {
  fechaActual: Date = new Date();
  arrPdf: any[] = [];
  cabeceras: number;
  ancho: string[];
  body: string[];
  pdf: any;

  constructor(private file: FileSaveService) {}
  //Inicio de exportar Periodo
  public exportPdfPersonal(json: any[], dias: number[], mes, anio) {
    const arrayFinal: any[] = this.formatJsonPersonal(json, dias);
    const docDefinition = {
      info: {
        title: `REPORTE COVID ${mes}-${anio}`,
        author: 'IDE Solution',
        subject: `Reporte de los trabajadores en ${mes}-${anio}`,
        keywords: 'Reporte',
      },
      pageOrientation: 'landscape',
      pageSize: 'A3',
      header: `Reporte del ${mes} de ${anio}`,
      content: [
        {
          text: `Reporte del ${mes} de ${anio}`,
          style: { fontSize: 15 },
        },
        {
          layout: 'lightHorizontalLines',
          table: {
            headerRows: 1,
            widths: this.getWidth(arrayFinal),
            body: this.buildBody(arrayFinal),
          },
        },
      ],
      defaultStyle: {
        fontSize: 5,
        characterSpacing: 0,
      },
    };
    const creado = pdfMake.createPdf(docDefinition);
    this.file.saveFile(creado, `Reporte_del_${mes}_de_${anio}`);
    this.ancho = [];
  }
  //Inicio de exportar Diario
  public exportPdfDiario(json: any[], dias, mes, anio, autor) {
    const arrayFinal: any[] = this.formatJsonDia(json);
    const docDefinition = {
      info: {
        title: `REPORTE COVID ${dias}/${mes}/${anio}`,
        author: 'IDE Solution',
        subject: `Reporte de los trabajadores en el día ${dias}/${mes}/${anio}`,
        keywords: 'Reporte',
      },
      pageOrientation: 'portrait',
      pageSize: 'A4',
      header: `Reporte generado el ${this.fechaActual.getDate()}/${
        this.fechaActual.getMonth() + 1
      }/${this.fechaActual.getFullYear()}  por ${autor}`,
      content: [
        {
          text: `Reporte del ${dias}/${mes}/${anio}`,
          style: { fontSize: 15, bold: true, alignment: 'center' },
        },
        {
          layout: 'lightHorizontalLines',
          table: {
            headerRows: 1,
            widths: this.getWidthDia(),
            body: this.buildBodyDia(arrayFinal),
          },
        },
      ],
      defaultStyle: {
        fontSize: 10,
        characterSpacing: 0,
      },
    };
    const creado = pdfMake.createPdf(docDefinition);
    this.file.saveFile(creado, `Reporte_del_${dias}-${mes}-${anio}`);

    this.ancho = [];
  }
  //Inicio de exportar Personal
  public exportPdfFromPersona(json: Personal) {
    const creado = pdfMake.createPdf(this.getWidthPersonal(json));
    this.file.saveFile(
      creado,
      `REPORTE_COVID_${json.fechaReporte.getDate()}-${
        json.fechaReporte.getMonth() + 1
      }-${json.fechaReporte.getFullYear()}`
    );
  }

  //PERIODOS
  private getWidth(json) {
    this.ancho = ['auto', '12%', 'auto'];
    this.cabeceras = Object.keys(json[0]).length;
    for (let index = 0; index < this.cabeceras - 3; index++) {
      this.ancho.push('2%');
    }
    return this.ancho;
  }
  private buildBody(json: any[]) {
    const body = [];
    const cabeceras = Object.keys(json[0]);
    const cabeceraFinal = cabeceras.map((element) =>
      element.replace('Día', '')
    );
    body.push(cabeceraFinal);
    json.forEach((element) => {
      body.push(Object.values(element));
    });
    return body;
  }
  private formatJsonPersonal(json: Periodo[], dias: any[]): any[] {
    this.arrPdf = [];
    let valorFinal = {};
    json.map((element) => {
      valorFinal = {
        DNI: element.dni,
        Nombre: element.nombre,
        Cargo: element.perfil,
      };
      dias.forEach((dia) => {
        if (element.dias.includes(dia)) {
          valorFinal['Día' + dia] = 'Sí';
        } else {
          valorFinal['Día' + dia] = 'No';
        }
      });
      this.arrPdf.push(valorFinal);
      valorFinal = {};
    });
    return this.arrPdf;
  }
  //DIARIO
  private formatJsonDia(json: any[]): any[] {
    this.arrPdf = [];
    json.map((element, index) =>
      this.arrPdf.push({
        Numero: index + 1,
        DNI: element.dni,
        Nombre: element.nombre,
        Cargo: element.perfil,
        Completado: element.estado !== 0 ? 'Sí' : 'No',
      })
    );
    return this.arrPdf;
  }
  private buildBodyDia(json: any[]) {
    const body = [];
    const cabeceras = Object.keys(json[0]);
    body.push(cabeceras);
    json.forEach((element) => {
      body.push(Object.values(element));
    });
    return body;
  }
  private getWidthDia() {
    this.ancho = ['10%', '10%', '45%', 'auto', 'auto'];
    return this.ancho;
  }
  //PERSONAL

  private getWidthPersonal(json: Personal) {
    json.fechaReporte = new Date(json.fechaReporte);
    const docDefinition = {
      info: {
        title: `REPORTE COVID ${json.fechaReporte.getDate()}/${
          json.fechaReporte.getMonth() + 1
        }/${json.fechaReporte.getFullYear()}`,
        author: 'IDE Solution',
        subject: `Reporte del trabajador en el día ${json.fechaReporte.getDate()}/${
          json.fechaReporte.getMonth() + 1
        }/${json.fechaReporte.getFullYear()}`,
        keywords: 'Reporte',
      },
      pageOrientation: 'portrait',
      pageSize: 'A4',
      content: this.contentPersonal(json),
      defaultStyle: {
        fontSize: 10,
        characterSpacing: 0,
      },
    };
    return docDefinition;
  }
  private contentPersonal(json: Personal) {
    const content = [
      {
        text: `Reporte del ${json.fechaReporte.getDate()}/${
          json.fechaReporte.getMonth() + 1
        }/${json.fechaReporte.getFullYear()}`,
        style: {
          fontSize: 15,
          bold: true,
          decoration: 'underline',
          alignment: 'center',
          lineHeight: '1.2',
        },
      },
      {
        columns: [
          {
            stack: [
              {
                text: `Datos de la persona \n`,
                style: {
                  fontSize: '14',
                  decoration: 'underline',
                  alignment: 'center',
                },
              },
              `Nombre: ${json.nombre} \n`,
              `Apellido: ${json.apellido} \n`,
              `Cargo: ${json.perfil} \n`,
              `Teléfono: ${json.telefono} \n`,
              `Correo: ${json.correo || 'No registra'} \n`,
              `Dirección: ${json.direccion || 'No registra'} \n`,
            ],
            style: { fontSize: 15, lineHeight: '1.2' },
          },
          {
            stack: this.regresarSintomas(json.sintomas),
            style: { fontSize: 15, lineHeight: '1.2', alignment: 'center' },
          },
        ],
      },
    ];
    content.push(this.regresarDatosVariables(json));
    return content;
  }
  private regresarSintomas(sintomas: string[]) {
    const valor = [];
    valor.push({
      text: `Datos de los Síntomas \n`,
      style: { fontSize: '14', decoration: 'underline', alignment: 'center' },
    });
    sintomas.forEach((data) => {
      valor.push(data);
    });
    return valor;
  }
  private regresarDatosVariables(json: Personal) {
    let datos;
    if (json.temperatura !== 0) {
      datos = [
        {
          columns: [
            {
              stack: this.regresarSignos(json.signos),
              style: { fontSize: 15, lineHeight: '1.2' },
            },
            {
              stack: [
                {
                  text: 'Datos del Reporte',
                  style: {
                    fontSize: 14,
                    decoration: 'underline',
                    alignment: 'center',
                    lineHeight: '1.2',
                  },
                },
                `Temperatura Ingresada: ${json.temperatura}\n`,
                `Comentario Adicional sobre la enfermedad:\n${json.comentario}`,
              ],
              style: { fontSize: 15, lineHeight: '1.2' },
            },
          ],
        },
      ];
    } else {
      datos = [
        {
          text: 'Datos del Reporte \n',
          style: {
            fontSize: '14',
            decoration: 'underline',
            alignment: 'center',
          },
        },
        {
          columns: [
            {
              stack: [
                `En la última semana ¿Ha tenido contacto con caso confirmado de Covid 19?\n ${
                  json.sospechozo ? 'Sí' : 'No'
                }`,
                `En la última semana ¿Ha tenido contacto con caso sospechoso de Covid 19?\n ${
                  json.confirmado ? 'Sí' : 'No'
                }`,
                `En la última semana ¿Ha dado positivo para Covid 19?\n ${
                  json.positivo ? 'Sí' : 'No'
                }`,
              ],
              style: { fontSize: 15, lineHeight: '1.2' },
            },
            {
              text: `Cantidad de Dosis de COVID 19:  ${json.cantDosis}`,
              style: { fontSize: 15, lineHeight: '1.2', alignment: 'center' },
            },
          ],
        },
      ];
    }
    return datos;
  }
  private regresarSignos(signos: string[]) {
    const valor = [];
    valor.push({
      text: `Datos de los Signos \n`,
      style: {
        fontSize: '14',
        decoration: 'underline',
        lineHeight: '1.2',
        alignment: 'center',
      },
    });
    signos.forEach((data) => {
      valor.push(data);
    });
    return valor;
  }
}
