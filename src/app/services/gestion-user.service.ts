import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import {
  Auth,
  ResponseInsert,
  Respuesta,
  ShowEdit,
} from '../models/auth.interface';
import { Perfil } from '../models/Perfil.interface';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root',
})
export class GestionUserService {
  trabajadores: Auth[];

  constructor(
    private toast: ToastController,
    private http: HttpClient,
    private token: TokenService
  ) {}

  agregarTrabajador(data) {
    return this.http.post<ResponseInsert>(
      `${
        environment.formularioEnvioUrl
      }usuario/insertUser/${this.token.getUserID()}`,
      data
    );
  }
  getListTrabajadores() {
    return this.http.get<Auth>(
      `${
        environment.formularioEnvioUrl
      }usuario/listarUsers/${this.token.getUserID()}`
    );
  }
  obtenerTipoTrabajador() {
    return this.http.get<Perfil[]>(
      `${
        environment.formularioEnvioUrl
      }session/tipoTra/${this.token.getUserID()}`
    );
  }
  eliminarTrabajador(id: number) {
    this.http
      .get<Respuesta>(
        `${
          environment.formularioEnvioUrl
        }usuario/eliminarUser/${id}/${this.token.getUserID()}`
      )
      .subscribe((data) => {
        if (data.codigo === 200) {
          this.presentToast('Usuario eliminado correctamente', 'success');
        } else {
          this.presentToast('Usuario no eliminado', 'dange');
        }
      });
  }
  editarTrabajador(data) {
    return this.http.post<Respuesta>(
      `${
        environment.formularioEnvioUrl
      }usuario/editarUserAdm/${this.token.getUserID()}`,
      data
    );
  }
  editarTrabajadorNor(data) {
    return this.http.post<Respuesta>(
      `${
        environment.formularioEnvioUrl
      }usuario/editarUser/${this.token.getUserID()}`,
      data
    );
  }
  obtenerTrabajarPorId(id) {
    return this.http.get<ShowEdit>(
      `${
        environment.formularioEnvioUrl
      }usuario/verUsuario/${id}/${this.token.getUserID()}`
    );
  }
  habilitar(id) {
    this.http
      .get<Respuesta>(
        `${
          environment.formularioEnvioUrl
        }usuario/habilitar/${id}/${this.token.getUserID()}`
      )
      .subscribe((data) => {
        if (data.codigo === 200) {
          this.presentToast('Usuario habilitado correctamente', 'success');
        } else {
          this.presentToast('Usuario no habilitado', 'dange');
        }
      });
  }
  deshabilitar(id) {
    this.http
      .get<Respuesta>(
        `${
          environment.formularioEnvioUrl
        }usuario/deshabilitar/${id}/${this.token.getUserID()}`
      )
      .subscribe((data) => {
        if (data.codigo === 200) {
          this.presentToast('Usuario deshabilitado correctamente', 'success');
        } else {
          this.presentToast('Usuario no deshabilitado', 'dange');
        }
      });
  }
  cambiarClaveUser(data) {
    return this.http.post<Respuesta>(
      `${
        environment.formularioEnvioUrl
      }usuario/editarContra/${this.token.getUserID()}`,
      data
    );
  }

  async presentToast(message, color) {
    const toast = await this.toast.create({
      message,
      color,
      position: 'top',
      duration: 1500,
    });
    toast.present();
  }
}
