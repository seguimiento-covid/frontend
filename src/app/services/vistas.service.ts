import { ToastController } from '@ionic/angular';
import { Diario } from 'src/app/models/Diario.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Personal } from '../models/Personal.interface';
import { Periodo } from '../models/Periodo.interface';
import { Perfil } from '../models/Perfil.interface';
import { Asignacion } from '../models/Asignacion.interface';
import { TokenService } from './token.service';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'any',
})
export class VistasService {
  trabajadoresPeriodo: Periodo[];
  trabajadoresDiario: Diario[];
  asignadores: Asignacion[];
  trabajadoresRango: Periodo[];
  constructor(
    private http: HttpClient,
    private toast: ToastController,
    private token: TokenService
  ) {}
  sendObjectSource(data: any) {
    window.localStorage.setItem('Dia', data.dia);
    window.localStorage.setItem('Mes', data.mes);
    window.localStorage.setItem('Anio', data.anio);
    window.localStorage.setItem('IdUs', data.idUser);
  }
  getObjectSource() {
    return {
      idUser: window.localStorage.getItem('IdUs'),
      dia: window.localStorage.getItem('Dia'),
      mes: window.localStorage.getItem('Mes'),
      anio: window.localStorage.getItem('Anio'),
    };
  }
  clearObjectSource() {
    window.localStorage.clear();
  }
  //HTTP METHODS
  obtenerPersonal(id: number, dia: number, mes: number, anio: number) {
    return this.http.get<Personal>(
      `${
        environment.formularioEnvioUrl
      }reporte/personal/${dia}/${mes}/${anio}/${id}/${this.token.getUserID()}`
    );
  }
  public retornarExcel(dia, mes, anio) {
    return this.http.get<Personal[]>(
      `${
        environment.formularioEnvioUrl
      }reporte/excel/${dia}/${mes}/${anio}/${this.token.getUserID()}`
    );
  }
  obtenerDiario(dia: string, mes: string, anio: string) {
    return this.http.get<Diario[]>(
      `${
        environment.formularioEnvioUrl
      }reporte/dia/${dia}/${mes}/${anio}/${this.token.getUserID()}`
    );
  }
  obtenerPeriodo(mes: number, anio: number) {
    return this.http.get<Periodo[]>(
      `${
        environment.formularioEnvioUrl
      }reporte/periodo/${mes}/${anio}/${this.token.getUserID()}`
    );
  }
  obtenerReportesHasCovid() {
    return this.http.get<Asignacion[]>(
      `${
        environment.formularioEnvioUrl
      }reporte/personas/covid/${this.token.getUserID()}`
    );
  }
  obtenerRangos(desde: string, hasta: string) {
    const arrDesde: string[] = desde.split('-');
    const arrHasta: string[] = hasta.split('-');
    return this.http.get<Periodo[]>(
      `${environment.formularioEnvioUrl}reporte/rango/${arrDesde[0]}/${
        arrDesde[1]
      }/${arrDesde[2]}/${arrHasta[0]}/${arrHasta[1]}/${
        arrHasta[2]
      }/${this.token.getUserID()}`
    );
  }
  obtenerTipoTrabajador() {
    return this.http.get<Perfil[]>(
      `${
        environment.formularioEnvioUrl
      }session/tipoTra/${this.token.getUserID()}`
    );
  }
  cambiarEstatus(asig: Asignacion) {
    asig.userMaster = Number(this.token.getUserID());
    return this.http.post<any>(
      `${environment.formularioEnvioUrl}reporte/covid/asignar`,
      asig
    );
  }

  sendPush(arry: string[]) {
    this.http
      .post<string>(`${environment.formularioEnvioUrl}push`, arry)
      .subscribe(
        (data) =>
          this.presentToast('Notificacion enviada correctamente!', 'success'),
        (err) => {
          console.log(err);
          this.presentToast('Error de envio', 'danger');
        }
      );
  }
  devolverPlataforma() {
    // return Capacitor.getPlatform() !== 'web';
    return false;
  }
  async presentToast(message, color) {
    const toast = await this.toast.create({
      message,
      color,
      position: 'top',
      duration: 1500,
    });
    toast.present();
  }
}
