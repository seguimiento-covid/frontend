import { Injectable } from '@angular/core';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { Capacitor } from '@capacitor/core';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
@Injectable({
  providedIn: 'root',
})
export class FileSaveService {
  constructor(private fileOpener: FileOpener) {}

  async saveFile(data, pathe: string) {
    if (Capacitor.getPlatform() === 'web') {
      data.download();
    } else {
      data.getBase64(async (dataRes) => {
        try {
          const path = `${pathe}.pdf`;
          const result = await Filesystem.writeFile({
            path,
            data: dataRes,
            directory: Directory.Documents,
            recursive: true,
          });
          this.fileOpener.open(`${result.uri}`, 'application/pdf');
        } catch (e) {
          console.log('No es posible', e);
        }
      });
    }
  }
}
