import { Injectable } from '@angular/core';
import * as Crypto from 'crypto-js';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EncrypterService {
  constructor() {}
  encrypt(dato) {
    return Crypto.AES.encrypt(dato, environment.secretKey);
  }
  desencrypt(dato) {
    if (dato !== null) {
      const print = Crypto.AES.decrypt(dato, environment.secretKey);
      return print.toString(Crypto.enc.Utf8);
    } else {
      return null;
    }
  }
}
