import { Capacitor } from '@capacitor/core';
/* eslint-disable @typescript-eslint/naming-convention */
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { Periodo } from '../models/Periodo.interface';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { Directory, Filesystem } from '@capacitor/filesystem';

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  arrExcel: any[];
  arregloSanos: any[];
  arregloCovid: any[];
  dateNow: Date = new Date();
  constructor(private datepipe: DatePipe, private fileOpener: FileOpener) {}
  //Dia
  // public exportToExcelDia(json: any[]) {
  //   const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(
  //     this.formatJsonDia(json)
  //   );
  //   ws['!cols'] = [
  //     { width: 10 },
  //     { width: 15 },
  //     { width: 30 },
  //     { width: 20 },
  //     { width: 12 },
  //   ];
  //   const wb: XLSX.WorkBook = this.asignarProps(
  //     `${this.datepipe.transform(this.dateNow, 'dd-MM-yyyy')}`
  //   );
  //   XLSX.utils.book_append_sheet(
  //     wb,
  //     ws,
  //     this.datepipe.transform(this.dateNow, 'dd-MM-yyyy')
  //   );
  //   XLSX.writeFile(
  //     wb,
  //     `Reporte de dia ${this.datepipe.transform(
  //       this.dateNow,
  //       'dd-MM-yyyy'
  //     )}.xlsx`
  //   );
  // }
  //Día
  exportToExcelDia(json: any[], pdf: any[], dia, mes, anio) {
    const wb: XLSX.WorkBook = this.asignarProps(`${dia}-${mes}-${anio}`);
    if (json.length !== 0) {
      this.formatJsonDia(json);
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.arregloSanos);
      ws['!cols'] = [
        { width: 10 },
        { width: 15 },
        { width: 30 },
        { width: 20 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
      ];

      const ws1: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.arregloCovid);
      ws1['!cols'] = [
        { width: 10 },
        { width: 15 },
        { width: 30 },
        { width: 20 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
        { width: 12 },
      ];
      XLSX.utils.book_append_sheet(
        wb,
        ws,
        'Sanos ' + this.datepipe.transform(json[0].fechaReporte, 'dd-MM-yyyy')
      );
      XLSX.utils.book_append_sheet(
        wb,
        ws1,
        'COVID ' + this.datepipe.transform(json[0].fechaReporte, 'dd-MM-yyyy')
      );
    }

    const ws2: XLSX.WorkSheet = XLSX.utils.json_to_sheet(
      this.formatJsonDiaHoja(pdf)
    );
    ws2['!cols'] = [
      { width: 10 },
      { width: 15 },
      { width: 30 },
      { width: 20 },
      { width: 12 },
    ];
    XLSX.utils.book_append_sheet(wb, ws2, `Completados ${dia}-${mes}-${anio}`);
    this.saveExcel(wb, `Reporte de dia ${dia}-${mes}-${anio}.xlsx`);
  }
  async saveExcel(data, name) {
    if (Capacitor.getPlatform() === 'web') {
      XLSX.writeFile(data, name);
    } else {
      const reporte = XLSX.write(data, { type: 'base64' });
      try {
        const path = `${name}`;
        const result = await Filesystem.writeFile({
          path,
          data: reporte,
          directory: Directory.Documents,
          recursive: true,
        });
        this.fileOpener.open(
          `${result.uri}`,
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );
      } catch (e) {
        console.log('No es posible', e);
      }
    }
  }
  //Rango
  public exportToExcelRango(json: any[], dias: string[], desde, hasta) {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(
      this.formatJsonRango(json, dias)
    );
    ws['!cols'] = [{ width: 15 }, { width: 35 }, { width: 20 }];
    dias.forEach(() => {
      ws['!cols'].push({ width: 7 });
    });
    const wb: XLSX.WorkBook = this.asignarProps(
      `rango ${this.datepipe.transform(this.dateNow, 'dd-MM-yyyy')}`
    );

    XLSX.utils.book_append_sheet(wb, ws, `Rango de ${desde}-${hasta}`);
    this.saveExcel(wb, `Reporte del Periodo ${desde}-${hasta}.xlsx`);
    // XLSX.writeFile(wb, `Reporte del Periodo ${desde}-${hasta}.xlsx`);
  }

  //Excel
  public exportToExcelPersonal(json: any[], dias: number[], mes, anio) {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(
      this.formatJsonPersonal(json, dias)
    );
    ws['!cols'] = [{ width: 15 }, { width: 35 }, { width: 20 }];
    dias.forEach(() => {
      ws['!cols'].push({ width: 6 });
    });
    const wb: XLSX.WorkBook = this.asignarProps(
      `periodico ${this.datepipe.transform(this.dateNow, 'dd-MM-yyyy')}`
    );

    XLSX.utils.book_append_sheet(wb, ws, `Periodo ${mes}-${anio}`);
    this.saveExcel(wb, `Reporte del Periodo ${mes}-${anio}.xlsx`);
    // XLSX.writeFile(wb, `Reporte del Periodo ${mes}-${anio}.xlsx`);
  }
  //Props
  private asignarProps(tipo: string): XLSX.WorkBook {
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    if (!wb.Props) {
      wb.Props = {};
    }
    wb.Props.Title = 'Reporte ' + tipo;
    wb.Props.Company = 'IDE Solution';
    return wb;
  }

  //Formateador del JSON por día
  private formatJsonDia(json: any[]) {
    this.arrExcel = [];
    this.arregloSanos = [];
    this.arregloCovid = [];
    json.map((element) => {
      if (element.normal) {
        this.arregloSanos.push({
          Numero: this.arregloSanos.length + 1,
          DNI: element.dni,
          Nombre: element.nombre,
          Apellido: element.apellido,
          Telefono: element.telefono,
          Cargo: element.perfil,
          Correo: element.correo,
          Direccion: element.direccion,
          Fecha: element.fechaReporte,
          Síntomas: this.regresarSintomas(element.sintomas),
          'Dosis de vacunas contra Covid 19 ': element.cantDosis,
          'En la última semana ¿Ha tenido contacto con caso sospechoso de Covid 19?':
            element.sospechozo ? 'Sí' : 'No',
          'En la última semana ¿Ha tenido contacto con caso confirmado de Covid 19? ':
            element.confirmado ? 'Sí' : 'No',
          'En la última semana ¿Ha dado positivo para Covid 19? ':
            element.positivo ? 'Sí' : 'No',
        });
      } else {
        this.arregloCovid.push({
          Numero: this.arregloCovid.length + 1,
          DNI: element.dni,
          Nombre: element.nombre,
          Apellido: element.apellido,
          Telefono: element.telefono,
          Cargo: element.perfil,
          Correo: element.correo,
          Direccion: element.direccion,
          Fecha: element.fechaReporte,
          Síntomas: this.regresarSintomas(element.sintomas),
          Signos: this.regresarSignos(element.signos),
          Temperatura: element.temperatura,
          Comentario: element.comentario,
        });
      }
    });
  }
  private formatJsonDiaHoja(json: any[]): any[] {
    const arrPdf = [];
    json.map((element, index) =>
      arrPdf.push({
        Numero: index + 1,
        DNI: element.dni,
        Nombre: element.nombre,
        Cargo: element.perfil,
        Completado: element.estado !== 0 ? 'Sí' : 'No',
      })
    );
    return arrPdf;
  }
  //Regresar Síntomas
  private regresarSintomas(sintomas: string[]) {
    let cadena = '';
    sintomas.forEach((element) => (cadena = cadena + element + ','));
    return cadena;
  }
  //Regresar Signos
  private regresarSignos(signos: string[]) {
    let cadena = '';
    signos.forEach((element) => (cadena = cadena + element + ','));
    return cadena;
  }
  //Formateador de JSON Personal
  private formatJsonPersonal(json: Periodo[], dias: any[]): any[] {
    this.arrExcel = [];
    let valorFinal = {};
    json.map((element) => {
      valorFinal = {
        DNI: element.dni,
        Nombre: element.nombre,
        Cargo: element.perfil,
      };
      dias.forEach((dia) => {
        if (element.dias.includes(dia)) {
          valorFinal['Día' + dia] = 'Sí';
        } else {
          valorFinal['Día' + dia] = 'No';
        }
      });
      this.arrExcel.push(valorFinal);
      valorFinal = {};
    });
    return this.arrExcel;
  }
  //Formateador de JSON Rango
  private formatJsonRango(json: Periodo[], dias: any[]): any[] {
    this.arrExcel = [];
    let valorFinal = {};
    json.map((element) => {
      valorFinal = {
        DNI: element.dni,
        Nombre: element.nombre,
        Cargo: element.perfil,
      };
      dias.forEach((dia, index) => {
        if (element.dias.includes(index)) {
          valorFinal[dia] = 'Sí';
        } else {
          valorFinal[dia] = 'No';
        }
      });
      this.arrExcel.push(valorFinal);
      valorFinal = {};
    });
    return this.arrExcel;
  }
}
