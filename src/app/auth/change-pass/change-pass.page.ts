import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.page.html',
  styleUrls: ['./change-pass.page.scss'],
})
export class ChangePassPage implements OnInit {
  formularioLogin: FormGroup;
  hideTemp = true;
  hideNew = true;
  hideRepeat = true;

  constructor(
    private fb: FormBuilder,
    private authSvc: AuthService,
    private router: Router,
    private token: TokenService
  ) {
    this.formularioLogin = this.fb.group({
      passwordTemp: ['', [Validators.required]],
      passwordNew: ['', [Validators.required]],
      repeatPass: ['', [Validators.required, this.matchValues('passwordNew')]],
    });
  }

  ngOnInit() {}
  ingresar() {
    if (this.formularioLogin.valid) {
      this.authSvc.changePass(
        this.token.getUserID(),
        this.formularioLogin.get('passwordTemp').value,
        this.formularioLogin.get('passwordNew').value
      );
    } else {
      this.authSvc.presentToast(
        'Complete de manera correcta los campos',
        'danger'
      );
      this.formularioLogin.markAllAsTouched();
    }
  }
  salir() {
    this.router.navigate(['/login']);
    this.authSvc.logout();
  }
  matchValues(matchTo: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null =>
      !!control.parent &&
      !!control.parent.value &&
      control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: false };
  }
}
