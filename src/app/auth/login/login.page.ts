import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formularioLogin: FormGroup;
  hide = true;

  constructor(private fb: FormBuilder, private authSvc: AuthService) {}

  ngOnInit() {
    this.formularioLogin = this.fb.group({
      dni: [
        '',
        [
          Validators.required,
          Validators.maxLength(8),
          Validators.minLength(7),
          Validators.pattern('^[0-9]*$'),
        ],
      ],
      password: ['', [Validators.required]],
      recordar: [false],
    });
  }
  async ingresar() {
    if (this.formularioLogin.valid) {
      await this.authSvc.login(this.formularioLogin.value);
      this.limpiarInputs();
      this.formularioLogin.markAsUntouched();
    } else {
      this.formularioLogin.markAllAsTouched();
    }
  }
  limpiarInputs() {
    this.formularioLogin.reset();

    // this.formularioLogin.patchValue({
    //   dni: [''],
    //   password: [''],
    //   recordar: [false],
    // });
  }
}
