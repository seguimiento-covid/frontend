import { SessionService } from './../services/session.service';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(
    private tokenService: SessionService,
    private token: TokenService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.token.getToken() !== null) {
      this.router.navigate(['/control/principal']);
      return new Promise((resolve) => {
        resolve(false);
        return;
      });
    }
    return this.tokenService.get('dni').then((data) => {
      if (data !== null) {
        this.tokenService.refreshToken();
        this.router.navigate(['/control/principal']);
        return new Promise((resolve) => {
          resolve(false);
          return;
        });
      } else {
        return new Promise((resolve) => {
          resolve(true);
          return;
        });
      }
    });
  }
}
