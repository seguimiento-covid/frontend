// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // formularioEnvioUrl: 'https://backend-qa.ide-solution.com:7040/api/',
  // formularioEnvioUrl: 'http://localhost:5000/api/',
  formularioEnvioUrl:
    'http://ec2-54-163-144-80.compute-1.amazonaws.com:6040/api/',
  // formularioEnvioUrl: 'http://192.168.0.28:5000/api/',
  version: '5.2.0',
  oneAppPushId: '95675270-9b2e-49e9-a5b5-29d765826e04',
  remitente: '571936918516',
  secretKey: '123*-+dasda',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
